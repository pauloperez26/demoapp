import { Component } from '@angular/core';

import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
    lists:any[]=[];
    deportes = ['Tenis','Baloncesto','Soccer'];
    items = [
       'Pokémon Yellow',
       'Super Metroid',
       'Mega Man X',
       'The Legend of Zelda',
       'Pac-Man',
       'Super Mario World',
       'Street Fighter II',
       'Half Life',
       'Final Fantasy VII',
       'Star Fox',
       'Tetris',
       'Donkey Kong III',
       'GoldenEye 007',
       'Doom',
       'Fallout',
       'GTA',
       'Halo'
     ];
  constructor(public navCtrl: NavController) {
    this.lists.push({
      nom:'Paulo', apel:'Perez Paternina', edad:36
    }, {
      nom:'Paula', apel:'Perez Vega', edad:0
    });

  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad HomePage');

      }
      itemSelected(item: string) {
         console.log("Selected Item", item);
       }
}
